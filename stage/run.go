package stage

import (
	"context"
	"fmt"
	"io/ioutil"
	"os/exec"

	"gitlab.com/ricardomendes/stderrout/logging"
)

type Run struct {
	ctx    context.Context
	logger logging.Logger

	pathToScript string
	subStage     string
}

func NewRunStage(
	ctx context.Context, logger logging.Logger,
	pathToScript string, subStage string) Run {

	runStage := &Run{
		ctx:    ctx,
		logger: logger.WithField("command", "run_exec"),

		pathToScript: pathToScript,
		subStage:     subStage,
	}

	return *runStage
}

func (r Run) Exec() error {
	r.logger.Info()
	fmt.Printf(`
	The RUN[%s] stage[sub stage] is processed here.
	Path to the generated script: %s
`, r.subStage, r.pathToScript)

	if r.subStage == "prepare_script" {
		err := r.printScript()
		if err != nil {
			return err
		}
		return r.executeScript()
	}

	fmt.Println(`
	The actual script will not be printed or executed to avoid exposing sensitive information!`)
	simulateScriptExecution(r.ctx)

	return nil
}

func (r Run) printScript() error {
	r.logger.Info("Reading the script...")

	script, err := ioutil.ReadFile(r.pathToScript)
	if err != nil {
		return fmt.Errorf("reading the script: %w", err)
	}

	fmt.Println(string(script))

	return nil
}

func (r Run) executeScript() error {
	r.logger.Info("Executing the script...")

	output, err := exec.Command(r.pathToScript).Output()
	if err != nil {
		return fmt.Errorf("executing the script: %w", err)
	}

	fmt.Println(string(output))

	return nil
}
