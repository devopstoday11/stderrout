package commands

import (
	"context"
	"fmt"

	"github.com/urfave/cli/v2"

	"gitlab.com/ricardomendes/stderrout/logging"
	"gitlab.com/ricardomendes/stderrout/stage"
)

// NewRunCommand builds the CLI abstraction for the "run" stage.
func NewRunCommand(ctx context.Context, logger logging.Logger) *cli.Command {
	return &cli.Command{
		Name:    "run",
		Aliases: []string{"run"},
		Usage:   "run a command in the RUN stage",

		Description: `
		Implementation of the RUN stage of the Custom Executor.
		This command will execute several scripts provided by GitLab Runner.

		The command receives two arguments:
		- pathToScript - path to the script to be executed
		- subStage - name of the sub stage

		Details on how to use this command can be found at
		https://docs.gitlab.com/runner/executors/custom.html#run.`,

		Action: func(c *cli.Context) error {
			pathToScript := c.Args().Get(0)
			subStage := c.Args().Get(1)

			err := stage.NewRunStage(ctx, logger, pathToScript, subStage).Exec()
			if err != nil {
				return fmt.Errorf("[RUN stage] %w", err)
			}

			return nil
		},
	}
}
