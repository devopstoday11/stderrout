package commands

import (
	"context"
	"fmt"

	"github.com/urfave/cli/v2"

	"gitlab.com/ricardomendes/stderrout/logging"
	"gitlab.com/ricardomendes/stderrout/stage"
)

// NewCleanupCommand builds the CLI abstraction for the "cleanup" stage.
func NewCleanupCommand(ctx context.Context, logger logging.Logger) *cli.Command {
	return &cli.Command{
		Name:    "cleanup",
		Aliases: []string{"cln"},
		Usage:   "run a command in the CLEANUP stage",

		Description: `
		Implementation of the CLEANUP stage of the Custom Executor.

		Details on how to use this command can be found at
		https://docs.gitlab.com/runner/executors/custom.html#cleanup.`,

		Action: func(c *cli.Context) error {
			err := stage.NewCleanupStage(ctx, logger).Exec()
			if err != nil {
				return fmt.Errorf("[CLEANUP stage] %w", err)
			}

			return nil
		},
	}
}
