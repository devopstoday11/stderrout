package commands

import (
	"context"
	"fmt"

	"github.com/urfave/cli/v2"

	"gitlab.com/ricardomendes/stderrout/logging"
	"gitlab.com/ricardomendes/stderrout/stage"
)

// NewPrepareCommand builds the CLI abstraction for the "prepare" stage.
func NewPrepareCommand(ctx context.Context, logger logging.Logger) *cli.Command {
	return &cli.Command{
		Name:    "prepare",
		Aliases: []string{"ppr"},
		Usage:   "run a command in the PREPARE stage",

		Description: `
		Implementation of the PREPARE stage of the Custom Executor.
				
		Details on how to use this command can be found at
		https://docs.gitlab.com/runner/executors/custom.html#prepare.`,

		Action: func(c *cli.Context) error {
			err := stage.NewPrepareStage(ctx, logger).Exec()
			if err != nil {
				return fmt.Errorf("[PREPARE stage] %w", err)
			}

			return nil
		},
	}
}
