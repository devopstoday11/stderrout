package commands

import (
	"context"
	"fmt"

	"github.com/urfave/cli/v2"

	"gitlab.com/ricardomendes/stderrout/logging"
	"gitlab.com/ricardomendes/stderrout/stage"
)

// NewConfigCommand builds the CLI abstraction for the "config" stage.
func NewConfigCommand(ctx context.Context, logger logging.Logger) *cli.Command {
	return &cli.Command{
		Name:    "config",
		Aliases: []string{"cfg"},
		Usage:   "run a command in the CONFIG stage",

		Description: `
		Implementation of the CONFIG stage of the Custom Executor.
		
		Details on how to use this command can be found at
		https://docs.gitlab.com/runner/executors/custom.html#config.`,

		Action: func(c *cli.Context) error {
			err := stage.NewConfigStage(ctx, logger).Exec()
			if err != nil {
				return fmt.Errorf("[CONFIG stage] %w", err)
			}

			return nil
		},
	}
}
