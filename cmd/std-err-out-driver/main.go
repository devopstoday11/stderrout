package main

import (
	"context"
	"fmt"
	"os"
	"strconv"

	"github.com/urfave/cli/v2"

	"gitlab.com/ricardomendes/stderrout"
	"gitlab.com/ricardomendes/stderrout/cmd/std-err-out-driver/commands"
	"gitlab.com/ricardomendes/stderrout/logging"
	"gitlab.com/ricardomendes/stderrout/signal"
)

func main() {
	logger := logging.New()
	logStartupMessage(logger)

	ctx := startSignalHandler(logger)

	app := &cli.App{
		Name:  stderrout.Name,
		Usage: stderrout.Usage,

		EnableBashCompletion: true,
		Commands: []*cli.Command{
			commands.NewConfigCommand(ctx, logger),
			commands.NewPrepareCommand(ctx, logger),
			commands.NewRunCommand(ctx, logger),
			commands.NewCleanupCommand(ctx, logger),
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Println("An unexpected ERROR has occurred!\n", err)
		buildFailureExitCode := os.Getenv("BUILD_FAILURE_EXIT_CODE")
		exitcode, _ := strconv.Atoi(buildFailureExitCode)
		os.Exit(exitcode)
	}
}

func logStartupMessage(logger logging.Logger) {
	logger.
		WithField("version", stderrout.Version().ShortLine()).
		Infof("Starting the \"%s\" driver!", stderrout.Name)
}

func startSignalHandler(logger logging.Logger) context.Context {
	terminationHandler := signal.NewTerminationHandler(logger)
	go terminationHandler.HandleSignals()

	return terminationHandler.Context()
}
