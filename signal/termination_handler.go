/* This is a simplified version of
https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/-/blob/master/internal/signal/termination_handler.go.

Special thanks to the GitLab Runner team for making it publicly available!*/

package signal

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/ricardomendes/stderrout/logging"
)

type TerminationHandler struct {
	ctx      context.Context
	cancelFn func()
	logger   logging.Logger
}

func NewTerminationHandler(logger logging.Logger) *TerminationHandler {
	ctx, cancelFn := context.WithCancel(context.Background())

	return &TerminationHandler{
		ctx:      ctx,
		cancelFn: cancelFn,
		logger:   logger,
	}
}

func (th *TerminationHandler) Context() context.Context {
	return th.ctx
}

func (th *TerminationHandler) HandleSignals() {
	stopCh := make(chan os.Signal)
	signal.Notify(stopCh, syscall.SIGINT, syscall.SIGTERM)

	sig := <-stopCh

	fmt.Println()
	th.logger.
		WithField("signal", sig).
		Warning("Received exit signal; bye!")

	th.cancelFn()
}
